<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Release extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'releases';

    protected $fillable = ['name', 'slug','receiver_user_id', 'create_user_id', 'details', 'description', 'active'];
}
