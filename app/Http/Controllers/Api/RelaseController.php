<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Release;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;

class RelaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $releasesRecived = Release::where('receiver_user_id', auth()->user()->id)->get();
        $releasesCreate = Release::where('create_user_id', auth()->user()->id)->get();

        $result = [
            'releaseRecived' => $releasesRecived,
            'releaseCreate' => $releasesCreate
        ];

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $release = new Release();
        $release->name = $request['name'];
        $release->slug = Str::slug($request['name']);
        $release->receiver_user_id = $request['user'];
        $release->details = $request['details'];
        $release->description = $request['description'];
        $release->create_user_id = auth()->user()->id;
        $release->save();

        return $release;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::get();
        return $users;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $release = Release::findOrFail($id);
        if($release->active){
            $release->active = false;
        } else {
            $release->active = true;
        }
        $release->save();

        return $release;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $release = Release::findOrFail($id);
        $release->delete();

        return $release;
    }
}
